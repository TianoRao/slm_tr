# Rust Serverless Transformer Endpoint Project

This project deploys a Hugging Face transformer model, implemented in Rust, as a serverless function on AWS Lambda. The function is containerized using Docker and is accessible via an HTTP endpoint.

## Requirements
- Docker
- AWS CLI, configured with appropriate permissions
- Rust and Cargo

## Getting Started

### Clone the Repository
Start by cloning the repository to your local machine:
```bash
git clone https://gitlab.com/TianoRao/slm_tr
cd slm_tr
```

### Building the Docker Container
Navigate to the project directory and build the Docker container:
```bash
docker buildx build --progress=plain --platform linux/arm64 -t <your project name> .
```

## Deploying to AWS Lambda

### Create an ECR Repository
First, create an ECR repository to store your Docker image:
```bash
aws ecr create-repository --repository-name <your repo name>
```

### Tag and Push Docker Image
Tag your Docker image to match the ECR repository, and push it:
```bash
docker tag <your project name> <ecr url>/<your repo name>
docker push <ecr url>/<your repo name>
```

### Deploy the Container to AWS Lambda
Deploy your container to AWS Lambda using the following command:
```bash
aws lambda create-function --function-name <your function name> \
  --package-type Image \
  --code ImageUri=<ecr url>/<your repo name> \
  --role <your IAM role ARN> \
  --timeout 15 --memory-size 3008
```

## Setting Up the Endpoint

### Create API Gateway Trigger
Create an API Gateway to expose the Lambda function as an HTTP endpoint:
```bash
aws apigatewayv2 create-api --name rustTransformerApi --protocol-type HTTP --target <arn of lambda>
```

## Usage

Once deployed, you can make requests to the endpoint using cURL:
```bash
curl -X POST "https://your-api-id.execute-api.us-east-1.amazonaws.com/" -d '{"input":"your query here"}'
```

## Rust Code Overview
The provided Rust code handles HTTP requests, performs model inference, and responds with the generated text. It utilizes the `lambda_http` crate to manage HTTP requests in a serverless Lambda environment, dynamically loads a transformer model, and processes inference requests efficiently.

### Key Components:
- `infer`: Function to perform model inference.
- `function_handler`: HTTP request handler.
- `main`: Entry point setting up the Lambda function to handle requests.

## Conclusion
This project demonstrates a practical implementation of serverless architecture with a machine learning model using Rust, Docker, and AWS Lambda, showcasing an efficient and scalable way to deploy AI-driven applications.

## Screenshots
![alt text](<imgs/Screenshot 2024-04-17 at 9.57.15 PM.png>)
![alt text](<imgs/Screenshot 2024-04-17 at 9.59.05 PM.png>)
![alt text](<imgs/Screenshot 2024-04-17 at 10.10.55 PM.png>)
